# KML Style Importer
Process KML files and import style definitions into QGIS (support is still partial).

## How it works
After importing a KML vector layer in QGIS there is a default style applied to that layer.

![](images/add_layer.png) ![](images/default_style.png)

This style differs from the one defined in the KML file:

![](images/kml_style.png)

With this plugin, you can use the styles defined in a KML file and apply them to your layer in QGIS.

First open the plugin, choose the KML file with the style you want to apply and select the layer you want to apply it to.

![](images/plugin_use_2.png) ![](images/plugin_style.png)

For KML files where the same folder has placemarks with different styles, the plugin provides the option of using the placemark names as a filter for the style definition. (Note: This option should be used with care for folders with a high number of placemarks)

Here we see one such case and the default style applied to the layer:

![](images/kml_style_2.png)![](images/default_style_2.png) 

Using the 'Use placemark names in style filters' option we are able to apply the desired style to the layer.

![](images/plugin_use_3.png) ![](images/plugin_style_2.png)
