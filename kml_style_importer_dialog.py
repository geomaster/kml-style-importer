# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KML Style Importer
 QGIS plugin to process KML files and import their style definitions
                             -------------------
        begin                : 2018-11-20
        copyright            : (C) 2018 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import os

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QDialog, QListWidgetItem, QMessageBox, QDialogButtonBox
from PyQt5.QtCore import Qt, QDir, pyqtSlot, QThread, pyqtSignal
from PyQt5.QtGui import QCursor

from qgis.core import QgsProject, QgsLayerTreeModel, QgsMapLayer, QgsLayerTreeLayer

from . import kml2sld

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ui/kml_style_importer_dialog.ui'))


class KmlStyleImporterDialog(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(KmlStyleImporterDialog, self).__init__(parent)
        self.setupUi(self)
        self.widget.setHidden(True)
        # TODO: a useful log
        self.showLogcheckBox.setHidden(True)
        self.buttonBox.button(
            QDialogButtonBox.Apply).clicked.connect(self.process)

    def showEvent(self, event):
        super(KmlStyleImporterDialog, self).showEvent(event)

        self.kmlRoot = None
        self.layerTreeRoot = QgsProject.instance().layerTreeRoot()

        self.model = QgsLayerTreeModel(self.layerTreeRoot)

        self.layerTree.setHeaderHidden(True)
        self.layerTree.setModel(self.model)
        self.layerTree.expandAll()

    def listKmlStyles(self, fpath):
        self.stylesList.clear()
        filePath = self.filePath.filePath()
        if os.path.splitext(filePath)[1] == '.kml':
            # Test if xml is valid
            self.kmlRoot, error, msg = kml2sld.load_kml_file(filePath)
            if error:
                QMessageBox.critical(None, "Error parsing file", msg)
                return

            layer_list, error, msg = kml2sld.get_styled_folders(self.kmlRoot)
            if error:
                QMessageBox.critical(None, "Error getting layer styles", msg)
                return

            for layer in layer_list:
                QListWidgetItem(layer, self.stylesList)
        else:
            QMessageBox.critical(None, "Invalid File",
                                 'File must be a kml file')

    def process(self):
        # check and validate selections
        selectedLayer = self.layerTree.selectedIndexes()[0] if len(
            self.layerTree.selectedIndexes()) > 0 else None

        selectedKmlStyle = self.stylesList.selectedItems()[0] if len(
            self.stylesList.selectedItems()) > 0 else None

        if not selectedLayer:
            emsg = "Must select a layer"
        else:
            if not selectedKmlStyle:
                emsg = "Must select a kml style"

        if not selectedLayer or not selectedKmlStyle:
            QMessageBox.critical(
                None, "Error applying style", emsg)
            return

        selectedTreeNode = self.model.index2node(selectedLayer)
        selectedKmlStyleName = selectedKmlStyle.text()

        selectedQgisLayer = None
        if not type(selectedTreeNode) is QgsLayerTreeLayer or not selectedTreeNode.layer().type() == QgsMapLayer.VectorLayer:
            QMessageBox.critical(
                None, "Error applying style", 'Invalid layer selection.\nMust select a vector layer.')
            return
        else:
            selectedQgisLayer = selectedTreeNode.layer()

        self.applyProcess = applyStyleProcess(
            self.kmlRoot, os.path.dirname(self.filePath.filePath()),
            selectedKmlStyleName, selectedQgisLayer, self.lnameFiltercheckBox.isChecked())

        self.applyProcess.finished.connect(self.finishedApplyStyle)

        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.applyProcess.start()

    def finishedApplyStyle(self):
        self.plainTextEdit.appendPlainText(
            "Finished applying style.\n*******\n")
        QApplication.restoreOverrideCursor()


class applyStyleProcess(QThread):
    signal = pyqtSignal('PyQt_PyObject')

    def __init__(self, kmlRoot, baseDir, selectedKmlStyleName, selectedQgisLayer, nameFilter):
        QThread.__init__(self)
        self.kmlRoot = kmlRoot
        self.baseDir = baseDir
        self.selectedKmlStyleName = selectedKmlStyleName
        self.selectedQgisLayer = selectedQgisLayer
        self.nameFilter = nameFilter

    def run(self):
        try:
            # get selected style
            if self.nameFilter:
                # Search for styles
                styles, names, error, msg = kml2sld.get_placemark_styles(
                    self.kmlRoot, self.selectedKmlStyleName)
                if error:
                    QMessageBox.critical(None, "Error applying style", msg)
                    return
            else:
                # Get folder style
                style, error, msg = kml2sld.get_style_for_folder(
                    self.kmlRoot, self.selectedKmlStyleName)
                if error or not style:
                    QMessageBox.critical(None, "Error applying style", msg)
                    return
                styles = [style]
                names = [self.selectedKmlStyleName]

            # convert to sld
            sld_string, error, msg = kml2sld.convert_to_sld(
                styles, names, self.baseDir, self.nameFilter, self.kmlRoot)
            if error:
                QMessageBox.critical(None, "Error applying style", msg)
                return

            # Write to temp file
            filename = QDir.tempPath() + "/style.sld"
            f = open(filename, 'wb')
            if os.path.exists(filename):
                f.write(sld_string)
                f.close()

            # print('apply style')
            msg_load, status = self.selectedQgisLayer.loadSldStyle(filename)
            if not status:
                QMessageBox.critical(None, "Error applying style", msg_load)
                return
            else:
                self.selectedQgisLayer.emitStyleChanged()
                self.selectedQgisLayer.triggerRepaint()

            # os.remove(filename)
        except Exception as e:
            QMessageBox.critical(None, "Error applying style", str(e))
