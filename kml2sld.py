# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KML Style Importer
 QGIS plugin to process KML files and import their style definitions
                             -------------------
        begin                : 2018-11-20
        copyright            : (C) 2018 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import os.path
import urllib.request
import xml.etree.ElementTree as ET
import re
import urllib.parse
import base64

from PyQt5.QtCore import QDir

ns = {
    'ns': 'http://www.opengis.net/kml/2.2'
}
point_style = 0
line_style = 1
poly_style = 2

def convert_kml_color(kml_color):
    """Converts a string representing a KML color (aabbggrr) to opacity
     plus an hex color (rrggbb)."""
    opacity = int(kml_color[:2], 16)
    hex_color = kml_color[6:8] + kml_color[4:6] + kml_color[2:4]
    return opacity/255.0, hex_color

def get_xml_element(xml_root, path, fltr, value, namespace):
    """Gets a xml element based on the given expression and filters"""
    expr = path
    if fltr and value:
        expr += '[' + fltr + '=\'' + value + '\']'

    result = None
    if xml_root is not None:
        result = xml_root.find(expr, namespace)

    return result

def load_kml_file(file_path):
    """Reads KML file and parses it into a etree doc."""
    result = None
    error = False
    msg = ''

    try:
        tree = ET.parse(file_path)
        result = tree.getroot()
    except ET.ParseError:
        error = True
        msg = 'File must be a valid xml file.'

    return result, error, msg

def get_styled_folders(xml_root):
    """Searches the KML for folders with styled elements"""
    result = []
    error = False
    msg = ''

    for layer in xml_root.findall(".//ns:Folder", ns):
        has_style_url = get_xml_element(layer, "./ns:Placemark/ns:styleUrl", None, None, ns)
        has_style = get_xml_element(layer, "./ns:Placemark/ns:Style", None, None, ns)
        if has_style_url is not None or has_style is not None:
            name = layer.find('ns:name', ns)
            result.append(name.text)

    return result, error, msg

def get_style_for_folder(xml_root, layer_name):
    """Searchs for folder with given name and retrieves first style found"""
    result = None
    error = False
    msg = ''

    layer = get_xml_element(xml_root, './/ns:Folder', 'ns:name', layer_name, ns)
    if layer is not None:
        style_url = get_xml_element(layer, 'ns:Placemark/ns:styleUrl', None, None, ns)
        if style_url is not None:
            style_id = re.match(r"^#(.*)", style_url.text).group(1)
            if style_id:
                ref = ''
                # is StyleMap ?
                # print(style_id)
                style_map = get_xml_element(xml_root, './ns:Document/ns:StyleMap', '@id', style_id, ns)
                if style_map:
                    ref_el = get_xml_element(style_map, 'ns:Pair[ns:key=\'normal\']/ns:styleUrl', None, None, ns)
                    if ref_el is not None:
                        ref = re.match(r"^#(.*)", ref_el.text).group(1)
                else:
                    ref = style_id
                # get Style
                # print(ref)
                result = get_xml_element(xml_root, './ns:Document/ns:Style', '@id', ref, ns)
        else:
            result = get_xml_element(layer, 'ns:Placemark/ns:Style', None, None, ns)
    if result is None:
        error = True
        msg = 'Error finding style'

    return result, error, msg

def get_placemark_styles(xml_root, folder_name):
    """Searchs for placemarks in folder with given name and retrieves styles found"""
    styles = []
    names = []
    error = False
    msg = ''

    placemarks = xml_root.findall('.//ns:Folder[ns:name=\''+folder_name+'\']/ns:Placemark', ns)
    style_index = 0
    if placemarks is not None:
        for layer in placemarks:
            style = None
            name = None

            style_url_element = get_xml_element(layer, 'ns:styleUrl', None, None, ns)
            if style_url_element is not None:
                style_id = re.match(r"^#(.*)", style_url_element.text).group(1)
                if style_id:
                    ref = ''
                    # is StyleMap ?
                    # print(style_id)
                    style_map = get_xml_element(xml_root, './ns:Document/ns:StyleMap', '@id', style_id, ns)
                    if style_map:
                        ref_el = get_xml_element(style_map, 'ns:Pair[ns:key=\'normal\']/ns:styleUrl', None, None, ns)
                        if ref_el is not None:
                            ref = re.match(r"^#(.*)", ref_el.text).group(1)
                    else:
                        ref = style_id
                    # get Style
                    # print(ref)
                    style = get_xml_element(xml_root, './ns:Document/ns:Style', '@id', ref, ns)
                    name = get_xml_element(layer, 'ns:name', None, None, ns)
            else:
                style = get_xml_element(layer, 'ns:Style', None, None, ns)
                name = get_xml_element(layer, 'ns:name', None, None, ns)

            styles.append(style)
            if name is not None:
                names.append(name.text)
            else:
                names.append('style'+str(style_index))
            style_index += 1

    return styles, names, error, msg

def style_has_effect(elem, kml_style, style_type):
    """Tests if a given kml style element has an effect on visual representation"""
    result = True

    if style_type == line_style:
        if elem == '{'+ns['ns']+'}LineStyle':
            line_color_element = get_xml_element(kml_style, 'ns:LineStyle/ns:color', None, None, ns)
            if line_color_element is None or line_color_element.text == '00000000':
                # print('linestyle has no effect')
                result = False

    if style_type == poly_style:
        if elem == '{'+ns['ns']+'}PolyStyle':
            poly_color_element = get_xml_element(kml_style, 'ns:PolyStyle/ns:color', None, None, ns)
            poly_fill = get_xml_element(kml_style, 'ns:PolyStyle/ns:fill', None, None, ns)
            if (poly_color_element is None or poly_color_element.text == '00000000') and (poly_fill is None):
                # print('polystyle has no effect')
                result = False

    return result

def create_polygon_sld(kml_style):
    """Creates SLD representation of a polygon"""
    error = False
    msg = ''

    polygon_symbolizer = ET.Element('PolygonSymbolizer')
    stroke = None

    poly_color_element = get_xml_element(kml_style, 'ns:PolyStyle/ns:color', None, None, ns)
    if poly_color_element is not None:
        poly_opacity, poly_color = convert_kml_color(poly_color_element.text)
        fill = ET.SubElement(polygon_symbolizer, 'Fill')
        fill_color = ET.SubElement(fill, 'CssParameter', {'name': 'fill'})
        fill_color.text = '#' + poly_color
        fill_opacity = ET.SubElement(fill, 'CssParameter', {'name': 'fill-opacity'})
        fill_opacity.text = str(round(poly_opacity, 2))

    line_color_element = get_xml_element(kml_style, 'ns:LineStyle/ns:color', None, None, ns)
    if line_color_element is not None:
        line_opacity, line_color = convert_kml_color(line_color_element.text)
        stroke = ET.SubElement(polygon_symbolizer, 'Stroke')
        stroke_color = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke'})
        stroke_color.text = '#' + line_color
        stroke_opacity = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke-opacity'})
        stroke_opacity.text = str(round(line_opacity, 2))

    line_width = get_xml_element(kml_style, 'ns:LineStyle/ns:width', None, None, ns)
    if line_width is not None:
        if stroke is None:
            stroke = ET.SubElement(polygon_symbolizer, 'Stroke')
        stroke_width = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke-width'})
        stroke_width.text = line_width.text

    # print(ET.tostring(polygon_symbolizer, encoding="utf-8", method='xml'))

    return polygon_symbolizer, error, msg

def create_point_eg_sld(kml_style, file_base):
    """Creates SLD representation of a point with an external graphic"""
    error = False
    msg = ''
    default_icon_size = 15 # 32
    default_point_size = 10
    default_point_color = '0000ff'

    point_symbolizer = ET.Element('PointSymbolizer')
    graphic = ET.SubElement(point_symbolizer, 'Graphic')

    # Get style properties
    icon_graphic = get_xml_element(kml_style, 'ns:IconStyle/ns:Icon/ns:href', None, None, ns)
    icon_scale = get_xml_element(kml_style, 'ns:IconStyle/ns:scale', None, None, ns)
    if icon_graphic is not None:
        image_format = 'image/' + os.path.splitext(icon_graphic.text)[1][1:]
        if not image_format == 'image/svg':
            filename = QDir.tempPath() + '/' + os.path.splitext(icon_graphic.text)[0].split('/')[-1] + '.svg'
            f = open(filename, 'wb')
            if os.path.exists(filename):
                imb = urllib.request.urlopen(icon_graphic.text).read()
                svgf = b'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
                svgf += b'<image width="100" height="100" xlink:href="data:image/png;base64,' + base64.b64encode(imb) + b'"/></svg>'
                f.write(svgf)
                f.close()
            image_format = 'image/svg+xml'
            external_graphic = ET.SubElement(graphic, 'ExternalGraphic')
            resource = ET.SubElement(external_graphic, 'OnlineResource', {'xlink:type': 'simple', 'xlink:href': filename})
            frmt = ET.SubElement(external_graphic, 'Format')
            frmt.text = image_format

            if icon_scale is not None:
                size = ET.SubElement(graphic, 'Size')
                size.text = str(int(default_icon_size*float(icon_scale.text)))
            else:
                size = ET.SubElement(graphic, 'Size')
                size.text = str(int(default_icon_size))
        else:
            image_format += '+xml'
            external_graphic = ET.SubElement(graphic, 'ExternalGraphic')
            resource = ET.SubElement(external_graphic, 'OnlineResource', {'xlink:type': 'simple', 'xlink:href': file_base+'/'+icon_graphic.text})
            frmt = ET.SubElement(external_graphic, 'Format')
            frmt.text = image_format

            if icon_scale is not None:
                size = ET.SubElement(graphic, 'Size')
                size.text = str(int(default_icon_size*float(icon_scale.text)))
            else:
                size = ET.SubElement(graphic, 'Size')
                size.text = str(int(default_icon_size))
    else:
        mark = ET.SubElement(graphic, 'Mark')
        wkn = ET.SubElement(mark, 'WellKnownName')
        wkn.text = 'circle'
        fill = ET.SubElement(mark, 'Fill')
        fill_color = ET.SubElement(fill, 'CssParameter', {'name': 'fill'})
        fill_color.text = '#' + default_point_color
        size = ET.SubElement(graphic, 'Size')
        size.text = str(int(default_point_size))


    # print(ET.tostring(point_symbolizer, encoding="utf-8", method='xml'))

    return point_symbolizer, error, msg

def create_point_lbl_sld(kml_style):
    """Creates SLD representation of a text label for point symbology"""
    error = False
    msg = ''

    text_symbolizer = ET.Element('TextSymbolizer')

    label = ET.SubElement(text_symbolizer, 'Label')
    label_prop = ET.SubElement(label, 'ogc:PropertyName')
    label_prop.text = 'name'

    label_color_element = get_xml_element(kml_style, 'ns:LabelStyle/ns:color', None, None, ns)
    label_scale = get_xml_element(kml_style, 'ns:LabelStyle/ns:scale', None, None, ns)
    if label_color_element is not None or label_scale is not None:
        font = ET.SubElement(text_symbolizer, 'Font')

        if label_color_element is not None:
            label_opacity, label_color = convert_kml_color(label_color_element.text)
            fill = ET.SubElement(text_symbolizer, 'Fill')
            fill_color = ET.SubElement(fill, 'CssParameter', {'name': 'fill'})
            fill_color.text = '#' + label_color

        if label_scale is not None:
            font_size = ET.SubElement(font, 'CssParameter', {'name': 'font-size'})
            font_size.text = str(int(10*float(label_scale.text)))

    # print(ET.tostring(text_symbolizer, encoding="utf-8", method='xml'))

    return text_symbolizer, error, msg

def create_line_sld(kml_style):
    """Creates SLD representation of a simple line symbology"""
    error = False
    msg = ''

    default_line_width = 1.0
    default_line_color = '000000'
    line_symbolizer = ET.Element('LineSymbolizer')

    line_color_element = get_xml_element(kml_style, 'ns:LineStyle/ns:color', None, None, ns)
    line_width = get_xml_element(kml_style, 'ns:LineStyle/ns:width', None, None, ns)
    if line_color_element is not None or line_width is not None:
        stroke = ET.SubElement(line_symbolizer, 'Stroke')

        if line_color_element is not None:
            line_opacity, line_color = convert_kml_color(line_color_element.text)
            stroke_color = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke'})
            stroke_color.text = '#' + line_color

        if line_width is not None:
            stroke_width = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke-width'})
            stroke_width.text = line_width.text
    else:
        stroke = ET.SubElement(line_symbolizer, 'Stroke')
        stroke_color = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke'})
        stroke_color.text = '#' + default_line_color
        stroke_width = ET.SubElement(stroke, 'CssParameter', {'name': 'stroke-width'})
        stroke_width.text = str(default_line_width)

    # print(ET.tostring(line_symbolizer, encoding="utf-8", method='xml'))

    return line_symbolizer, error, msg

def identify_feature_type(elem):
    """Identifies what type of feature is represented by kml element"""
    tipe = -1

    point = get_xml_element(elem, 'ns:Point', None, None, ns)
    line_string = get_xml_element(elem, 'ns:LineString', None, None, ns)
    if point is not None:
        tipe = point_style
    elif line_string is not None:
        tipe = line_style

    return tipe

def convert_to_sld(styles, layers, file_base, multiple, xml_root):
    """Creates SLD from the given KML style elements"""
    result = None
    error = False
    msg = ''

    # Create sld
    sld = ET.Element('StyledLayerDescriptor', {'version': '1.0.0',
                                               'xsi:schemaLocation': 'http://www.opengis.net/sld StyledLayerDescriptor.xsd',
                                               'xmlns': 'http://www.opengis.net/sld',
                                               'xmlns:ogc': 'http://www.opengis.net/ogc',
                                               'xmlns:xlink': 'http://www.w3.org/1999/xlink',
                                               'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance'})
    named_layer = ET.SubElement(sld, 'NamedLayer')
    name = ET.SubElement(named_layer, 'Name')
    name.text = 'KML Style Importer by Geomaster'
    user_style = ET.SubElement(named_layer, 'UserStyle')
    feature_type_style = ET.SubElement(user_style, 'FeatureTypeStyle')

    for kml_style, layer_name in list(zip(styles, layers)):
        rule = ET.SubElement(feature_type_style, 'Rule')
        if multiple is True:
            fltr = ET.SubElement(rule, 'ogc:Filter')
            cond = ET.SubElement(fltr, 'ogc:PropertyIsEqualTo')
            cond_field = ET.SubElement(cond, 'ogc:PropertyName')
            cond_field.text = 'name'
            cond_value = ET.SubElement(cond, 'ogc:Literal')
            cond_value.text = layer_name

        tipe = -1
        if kml_style is not None:
            # compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
            # Check style type
            tag_list = [elem.tag for elem in kml_style.iter()]
            # polygons
            if all(elem in tag_list and style_has_effect(elem, kml_style, 2) for elem in [
                    '{'+ns['ns']+'}LineStyle', '{'+ns['ns']+'}PolyStyle']):
                # print('polystyle')
                poly, error, msg = create_polygon_sld(kml_style)
                rule.append(poly)
                result = ET.tostring(sld, encoding="utf-8", method='xml')
            # points
            elif all(elem in tag_list and style_has_effect(elem, kml_style, 0) for elem in [
                    '{'+ns['ns']+'}IconStyle']):
                # print('pointstyle')
                label, error, msg = create_point_lbl_sld(kml_style)
                point, error, msg = create_point_eg_sld(kml_style, file_base)
                rule.append(point)
                rule.append(label)
                result = ET.tostring(sld, encoding="utf-8", method='xml')
            # lines
            elif all(elem in tag_list and style_has_effect(elem, kml_style, 1) for elem in [
                    '{'+ns['ns']+'}LineStyle']):
                # print('linestyle')
                line, error, msg = create_line_sld(kml_style)
                rule.append(line)
                result = ET.tostring(sld, encoding="utf-8", method='xml')
            else:
                error = True
                msg = 'Error applying KML style. Unsupported style.'
                print(tag_list)
        else:
            layer = get_xml_element(xml_root, './/*', 'ns:name', layer_name, ns)
            tipe = identify_feature_type(layer)
            if tipe == point_style:
                label, error, msg = create_point_lbl_sld(kml_style)
                point, error, msg = create_point_eg_sld(kml_style, file_base)
                rule.append(point)
                rule.append(label)
                result = ET.tostring(sld, encoding="utf-8", method='xml')
            if tipe == line_style:
                line, error, msg = create_line_sld(kml_style)
                rule.append(line)
                result = ET.tostring(sld, encoding="utf-8", method='xml')

    # print(result)
    # print(error)
    # print(msg)

    return result, error, msg
